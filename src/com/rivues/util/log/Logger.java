package com.rivues.util.log;

/**
 * 
 * @author admin
 *
 */
public class Logger {
	private org.slf4j.Logger log = null ;
	
	public Logger(Class<?>clazz){
		log = org.slf4j.LoggerFactory.getLogger(clazz) ;
	}
	public void debug(String msg , String orgi , String flowid){
		log.debug(new StringBuffer().append("[").append(orgi).append("/").append(flowid).append("] ").append(msg).toString()) ;
	}
	public void info(String msg , String orgi , String flowid){
		log.info(new StringBuffer().append("[").append(orgi).append("/").append(flowid).append("] ").append(msg).toString()) ;
	}
	public void warn(String msg , String orgi , String flowid){
		log.warn(new StringBuffer().append("[").append(orgi).append("/").append(flowid).append("] ").append(msg).toString()) ;
	}
	public void error(Throwable error , String orgi , String flowid){
		log.error(new StringBuffer().append("[").append(orgi).append("/").append(flowid).append("] ").append(error.getMessage()).toString() , error) ;
	}
}
