package com.rivues.util.session;

import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;

public class R3WebSessionManager extends DefaultWebSessionManager {
	private long globalSessionTimeout ;
	public R3WebSessionManager() {
		super();
	}
	@Override
	public long getGlobalSessionTimeout() {
		return globalSessionTimeout;
	}
	public void setGlobalSessionTimeout(long globalSessionTimeout) {
		this.globalSessionTimeout = globalSessionTimeout;
	}
	
}
