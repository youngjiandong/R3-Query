package com.rivues.util.service.business;


import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;

import java_cup.simple_calc.sym;

import javax.sql.DataSource;

import mondrian.olap.DriverManager;
import mondrian.olap.Util;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.module.platform.web.model.Database;
import com.rivues.util.datasource.CubeTools;
import com.rivues.util.datasource.DataSourceTools;
import com.rivues.util.service.ServiceHelper;
import com.rivues.util.service.monitor.BusinessService;

/**
 * 
 * @author admin
 *
 */
@Service("datasource")
public class DataSourceService extends BusinessService{

	@Override
	public String getName() {
		return RivuDataContext.ServiceTypeName.DATASOURCE.toString();
	}

	@Override
	public void service() throws Exception {
		
	}
	/**
	 * 使用线程同步，避免多个 线程同时进入，导致抛出异常
	 * @param cube
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	private synchronized DataSource getDataSource(Cube cube) throws NoSuchAlgorithmException, SQLException{
		/**
		 * 获取数据库链接，使用连接池方式
		 */
		/**
		 * 使用线程同步，避免多个 线程同时进入，导致抛出异常
		 */
		DataSource ds = null ;
		if("true".equals(cube.getCreatedata())){
			ds = DataSourceTools.getDataSource(RivuDataContext.R3_SYSTEM) ;
			if(ds==null){
				List<Database> databaseList = RivuDataContext.getService().findPageByCriteria(DetachedCriteria.forClass(Database.class).add(Restrictions.eq("name", RivuDataContext.R3_SYSTEM))) ;
				if(databaseList.size()>0){
					ds = DataSourceTools.createDataSource(databaseList.get(0) ) ;
				}
			}
		}else{
			ds = DataSourceTools.getDataSource(cube.getDb()) ;
		}
		if(ds==null){
			ds = DataSourceTools.createDataSource((Database) RivuDataContext.getService().getIObjectByPK(Database.class, cube.getDb())) ;
		}
		return ds ;
	}
	/**
	 * 
	 * @param cube
	 * @throws Exception
	 */
	public Object service(Cube cube , String fileName) throws Exception {
		Object dataSourceObject = null ;
		StringBuffer strb = new StringBuffer();
		DataSource ds = getDataSource(cube) ;//获取连接池
		if(cube!=null && RivuDataContext.CubeEnum.CUBE.toString().equals(cube.getDstype())){
			Util.PropertyList properties = null ;
			if(cube.getCreatedata()!=null && cube.getCreatedata().equals("true")){
				properties = Util.parseConnectString(strb.append("Provider=mondrian;")
						.append(
					       "Catalog=").append(fileName).append(";").toString());
			}else{
				properties = Util.parseConnectString(strb.append("Provider=mondrian;")
						.append(
					       "Catalog=").append(fileName).append(";").toString());
			}
			if(properties!=null){
				dataSourceObject = DriverManager.getConnection(properties,null , ds) ;
			}
		}else if(cube!=null && RivuDataContext.CubeEnum.TABLE.toString().equals(cube.getDstype())){
			if(ds!=null){
				dataSourceObject = ds.getConnection() ;
			}
		}
		return dataSourceObject ;
	}


}
