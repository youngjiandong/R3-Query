package com.rivues.util.tools;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rivues.module.account.web.handler.LoginController;

public class FileUtil {
	/**
	 * 批量删除文件
	 * @param filenames 文件路径（E://file/file.txt）
	 */
	public static void deleteFiles(List<String> filenames)throws Exception{
		if(filenames==null){
			return;
		}
		File file = null;
		for (String filename : filenames) {//删除创建的临时文件
			file = new File(filename);
			if(file.exists()){
				file.delete();
			}
		}
	}
	
	public static void deleteFile(String filename)throws Exception{
		if(StringUtils.isBlank(filename)){
			return;
		}
		
		File file = new File(filename);
		if(file.exists()){
			file.delete();
		}
	}
}
