package com.rivues.util.tools;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.rivues.module.platform.web.model.AnalyzerReport;
import com.rivues.module.platform.web.model.AnalyzerReportModel;
import com.rivues.module.platform.web.model.JobDetail;
import com.rivues.util.data.ReportData;

public interface ExportFile {
	public String getHeadTitle() ;
	public String getStartTime() ;
	public String getEndTime() ;
	
	public void setHeadTitle(String title) ;
	public void setStartTime(String startTime) ;
	public void setEndTime(String endtime);
	
	public void setJobDetail(JobDetail job);
	
	public JobDetail getJobDetail();
	
	public void createFile(boolean limit) throws Exception;
	
	public void setPage(int page) throws Exception;
	
	public int getPage() throws Exception;
	
	public void close();
	
	public void createFile(String filename) throws Exception;
	
	public void writeHead(ReportData reportData) throws Exception ;
	
	public void writeRow(ReportData reportData) throws Exception;
	/**
	 * 创建sheet页
	 * @param sheetName sheet页name
	 * @throws Exception
	 */
	public void createSheet(String sheetName)throws Exception;
	
	public ReportData getReportData() ;
	
	public void setReportData(ReportData reportData)throws Exception;
	
	public void setModel(AnalyzerReportModel model)throws Exception;
	
	public void setReport(AnalyzerReport report)throws Exception;
	
	public void setRowNum(int rowNum)throws Exception;
	
	public int getRowNum()throws Exception;
	
	public void setOut(OutputStream out)throws Exception;
}
