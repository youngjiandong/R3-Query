package com.rivues.util.iface.report;

public class ReportFactory {

	private static Report report ;
	/**
	 * 
	 * @return
	 */
	public static Report getReportInstance(){
		return report!=null ? report : (report = new DatabaseReportImpl()) ;
	}
}
