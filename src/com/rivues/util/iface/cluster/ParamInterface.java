package com.rivues.util.iface.cluster;

import com.rivues.module.platform.web.model.ConfigureParam;

/**
 * 从集群获取参数 ， 1、参数保存在数据库中， 2、参数保存在 EhCache 中， 3、参数保存在Hazlcast 中
 * @author admin
 *
 */
public interface ParamInterface {
	/**
	 * 
	 * @param param
	 * @return
	 */
	public String getParam(String param) ;
	
	/**
	 * 根据name获取对象
	 * @param name
	 * @return
	 */
	public ConfigureParam getConfigureParam(String name);
}
