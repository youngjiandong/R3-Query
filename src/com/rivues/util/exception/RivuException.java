package com.rivues.util.exception;

import java.util.Date;

import com.rivues.util.RivuTools;
import com.rivues.util.service.cache.CacheHelper;

/**
 * 获取属性文件信息异常
 * @author Administrator
 *
 */
public class RivuException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8167471906939988764L;
	
	private String errcode = "ERROR001";
	private String errtime = RivuTools.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss");
	private String errmsg = "未知异常";
	
	public RivuException(String errcode){
		super(errcode);
		Object msg = CacheHelper.getDistributedDictionaryCacheBean().getCacheObject(errcode, "rivues") ;
		if(msg!=null){
			this.errmsg = msg.toString();
		}
		this.errcode = errcode;
		this.errtime = RivuTools.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss");
	}

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrtime() {
		return errtime;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	
	
	
}
