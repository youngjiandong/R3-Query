package com.rivues.api.util;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;


public class R3HttpTools {
	/***
	 * 发送 HTTP 请求
	 * 
	 * @param url
	 *            HTTP 请求地址
	 * @param data
	 *            HTTP 传输对象
	 * @return string HTTP 返回结果
	 * @throws Exception
	 */
	public static String sendHttp(String url, NameValuePair[] data) throws Exception {
		String str = "";
		try {
			PostMethod method = new UTF8PostMethod(url);

			method.setRequestBody(data);

			HttpClient httpClient = new HttpClient();

			httpClient.executeMethod(method);

			str = method.getResponseBodyAsString();

			method.releaseConnection();

		} catch (Exception e) {
			throw e;
		}
		return str;
	}
}
