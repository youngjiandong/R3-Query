package com.rivues.module.platform.web.model;

import org.apache.solr.client.solrj.SolrQuery;

import com.rivues.util.RivuTools;
import com.rivues.util.datasource.ConditionExpress;

public class QueryText implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2725202491180562487L;
	private String queryText ;
	private String nativeQueryText ;
	private ConditionExpress express ;
	private String schema_version ;
	
	private int p = 1;
	private int ps = 20;
	private String key ;
	private SolrQuery query ; 
	private boolean drillThrough ;
	
	public SolrQuery getQuery() {
		return query;
	}
	public void setQuery(SolrQuery query) {
		this.query = query;
	}
	public String getQueryText() {
		return queryText;
	}
	public void setQueryText(String queryText) {
		this.queryText = queryText;
	}
	public int getP() {
		return p;
	}
	public void setP(int p) {
		this.p = p;
	}
	public int getPs() {
		return ps;
	}
	public void setPs(int ps) {
		this.ps = ps;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getNativeQueryText() {
		return nativeQueryText;
	}
	public void setNativeQueryText(String nativeQueryText) {
		this.nativeQueryText = nativeQueryText;
	}
	public boolean isDrillThrough() {
		return drillThrough;
	}
	public void setDrillThrough(boolean drillThrough) {
		this.drillThrough = drillThrough;
	}
	public ConditionExpress getExpress() {
		return express;
	}
	public void setExpress(ConditionExpress express) {
		this.express = express;
	}
	public String getSchema_version() {
		return schema_version;
	}
	public void setSchema_version(String schema_version) {
		this.schema_version = schema_version;
	}
}
