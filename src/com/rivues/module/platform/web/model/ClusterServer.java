package com.rivues.module.platform.web.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.rivues.util.RivuTools;

@Entity
@Table(name = "rivu5_clusterserver")
@org.hibernate.annotations.Proxy(lazy = false)
public class ClusterServer implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id ;
	private String name ;
	private String code ;
	private String orgi ;
	private String userid ;
	private String serverhost ;
	private int port;
	private String spare1;//服务 1启动 ，0关闭
	private String spare0;//jvm 1启动，0关闭
	private Date createtime;
	private String groupid;
	private String zkhost;
	private String hostarea ;
	private String clustertype ;
	private int servers;//服务器健康程度  大于等于8健康，大于等于5小于8良好，大于等于三小于五警告，小于三危险，等于0严重
	private int searchcores;
	private boolean back;
	private boolean dispatch;
	private boolean analyse;
	private boolean exchange;
	private boolean others;

	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getServerhost() {
		return serverhost;
	}
	public void setServerhost(String serverhost) {
		this.serverhost = serverhost;
	}
	public String getSpare1() {
		return spare1;
	}
	public void setSpare1(String spare1) {
		this.spare1 = spare1;
	}
	public String getSpare0() {
		return spare0;
	}
	public void setSpare0(String spare0) {
		this.spare0 = spare0;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getZkhost() {
		return zkhost;
	}
	public void setZkhost(String zkhost) {
		this.zkhost = zkhost;
	}
	public String getHostarea() {
		return hostarea;
	}
	public void setHostarea(String hostarea) {
		this.hostarea = hostarea;
	}
	public String getClustertype() {
		return clustertype;
	}
	public void setClustertype(String clustertype) {
		this.clustertype = clustertype;
	}
	public int getServers() {
		return servers;
	}
	public void setServers(int servers) {
		this.servers = servers;
	}
	public int getSearchcores() {
		return searchcores;
	}
	public void setSearchcores(int searchcores) {
		this.searchcores = searchcores;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public boolean isBack() {
		return back;
	}
	public void setBack(boolean back) {
		this.back = back;
	}
	public boolean isDispatch() {
		return dispatch;
	}
	public void setDispatch(boolean dispatch) {
		this.dispatch = dispatch;
	}
	public boolean isAnalyse() {
		return analyse;
	}
	public void setAnalyse(boolean analyse) {
		this.analyse = analyse;
	}
	public boolean isExchange() {
		return exchange;
	}
	public void setExchange(boolean exchange) {
		this.exchange = exchange;
	}
	public boolean isOthers() {
		return others;
	}
	public void setOthers(boolean others) {
		this.others = others;
	}
	
}
