package com.rivues.module.platform.web.model.chart;

import java.util.ArrayList;
import java.util.List;

import com.rivues.util.RivuTools;
import com.rivues.util.data.FirstTitle;
import com.rivues.util.data.ReportData;
import com.rivues.util.data.ValueData;

public class DataSeries implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3149405007942992250L;
	
	private String id ;
	private String type = "y";
	private String name ;
	private String data ;
	private String start ;	//数据范围
	private String chartype ;
	private String end ;
	private int datainx = 0 ; 	//数据 index
	private String left ;
	private String top;
	private String size ;
	private boolean datalabel ;
	
	
	public DataSeries(){
		initDataSeries();
	}
	public void initDataSeries(){
		if(customdata!=null){
			String[] datas = customdata.split(",");
			for(String data:datas){
				if(data.indexOf(":")>=0){
					String[] labelData = data.split(":") ;
					if(labelData.length==2){
						LabelData curData = new LabelData();
						curData.setLabel(labelData[0]) ;
						curData.setValue(labelData[1]) ;
						custom.add(curData) ;
					}else{
						LabelData curData = new LabelData();
						curData.setLabel(labelData[0]) ;
						curData.setValue(labelData[0]) ;
						custom.add(curData) ;
					}
				}
			}
		}
	}
	/**
	 * 自定义数据，以下为测试数据
	 */
	private String customdata = "Twitter:Twitter,Google:Google,FaceBook:FaceBook,SINA:SINA,Tencent:Tencent";
	private List<LabelData> custom = new ArrayList<LabelData>();
	
	public List<LabelData> getCustom(){
		return custom ;
	}
	
	public void setCustom(List<LabelData> custom) {
		this.custom = custom;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	public String getCustomdata() {
		return customdata;
	}
	public void setCustomdata(String customdata) {
		this.customdata = customdata;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getRange(){
		StringBuffer strb = new StringBuffer();
		if(this.getStart()!=null && this.getEnd()!=null){
			
			String colinx = start.substring(0, start.indexOf(":")) ;
			String startrowinx = start.substring(start.indexOf(":")+1 ) ;
			String endrowinx = end.substring(end.indexOf(":")+1 ) ;
			int colindex = Integer.parseInt(colinx) ;
			int startrowindex = Integer.parseInt(startrowinx) ;
			int endrowindex = Integer.parseInt(endrowinx) ;
			
			strb.append(RivuTools.getInx2Char(colindex)).append(":").append(startrowinx).append(" - ").append(RivuTools.getInx2Char(colindex)).append(":").append(endrowindex) ;
		}
		return strb.length()>0 ? strb.toString() : null ;
	}
	
	public List<ValueData> values(ReportData reportData){
		List<ValueData> valueDataList = new ArrayList<ValueData>() ;
		if(reportData!=null && reportData.getData()!=null){
			if(this.getStart()!=null && this.getEnd()!=null){
				
				String colinx = start.substring(0, start.indexOf(":")) ;
				String startrowinx = start.substring(start.indexOf(":")+1 ) ;
				String endrowinx = end.substring(end.indexOf(":")+1 ) ;
				int colindex = Integer.parseInt(colinx) ;
				int startrowindex = Integer.parseInt(startrowinx) ;
				int endrowindex = Integer.parseInt(endrowinx) ;
				
				for(int i=startrowindex ; i< reportData.getData().size() && i <= endrowindex ; i++){
					if(colindex < reportData.getData().get(i).size()){
						valueDataList.add( reportData.getData().get(i).get(colindex)) ;
					}
				}
			}else{
				for(int inx = 0 ; inx < reportData.getData().get(0).size() ; inx++){
					ValueData vd = reportData.getData().get(0).get(inx) ;
					if(vd.getName().equals(this.getData()) && inx == this.datainx){
						for(List<ValueData> vdList : reportData.getData()){
							if(vdList.size()>inx){
								valueDataList.add( vdList.get(inx)) ;
							}
						}
						break ;
					}
				}
				
			}
		}
		return valueDataList ;
		
	}
	public int getDatainx() {
		return datainx;
	}
	public void setDatainx(int datainx) {
		this.datainx = datainx;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getChartype() {
		return chartype;
	}
	public void setChartype(String chartype) {
		this.chartype = chartype;
	}
	public String getLeft() {
		return left;
	}
	public void setLeft(String left) {
		this.left = left;
	}
	public String getTop() {
		return top;
	}
	public void setTop(String top) {
		this.top = top;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public boolean isDatalabel() {
		return datalabel;
	}
	public void setDatalabel(boolean datalabel) {
		this.datalabel = datalabel;
	}
}
