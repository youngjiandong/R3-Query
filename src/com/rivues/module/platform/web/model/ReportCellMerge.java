package com.rivues.module.platform.web.model;

public class ReportCellMerge implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6825355624632960117L;
	private String id ;
	private String start ;
	private String end ;
	private String type = "cellmerge"	;	//JAVA Bean处理类型
	private String mergetype ;		//合并后的处理，求和（sum）/平均数 （avg）/最大值（max）/最小值（min）
	private String format ;			//数字格式化字符串 ， ##,##.##
	public ReportCellMerge(){}
	public ReportCellMerge(String start , String end , String mergetype){
		this.start = start ; 
		this.end = end ;
		this.mergetype = mergetype ;
	}
	public void setStartRow(int startRow){}
	public void setStartCol(int startCol){}

	public void setEndRow(int startRow){}
	public void setEndCol(int startCol){}
	public int getStartRow(){
		return Integer.parseInt(start!=null ? start.substring(start.indexOf(":")+1):"-1") ;
	}
	public int getStartCol(){
		return Integer.parseInt(start!=null ? start.substring(0, start.indexOf(":")):"-1") ;
	}
	public int getEndRow(){
		return Integer.parseInt(end!=null ? end.substring(end.indexOf(":")+1):"-1") ;
	}
	public int getEndCol(){
		return Integer.parseInt(end!=null ? end.substring(0, end.indexOf(":")):"-1") ;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getMergetype() {
		return mergetype;
	}
	public void setMergetype(String mergetype) {
		this.mergetype = mergetype;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
}
