package com.rivues.module.report.web.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rivues.module.platform.web.model.ChartProperties;
import com.rivues.module.platform.web.model.SearchResultTemplet;
import com.rivues.module.platform.web.model.chart.DataSeries;
import com.rivues.module.platform.web.model.chart.LayoutData;
import com.rivues.util.service.cache.CacheHelper;

public class ModelPackage implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4321262368741004957L;
	
	private String id ;
	private String name ;
	private String title ;
	private String templetid ;
	private String width ;
	private String widthunit ;
	private String height ;
	private String heightunit ;
	private String orgi ;
	private String pkgtype ;//图表  OR ReportModel
	private String type ;	//类型 ： layout table model chart
	private boolean pkglayout ;
	private List<List<LayoutData>>	layoutcontent ;			//如果元素类型为 布局，则此 参数有效
	private ChartProperties chart ;
	private boolean displaytitle  = true ;
	private String htmlcontent;
	
	private Map<String , Object> values = new HashMap<String, Object>();	//从设计器界面提交过来的 动态参数
	/**
	 * 数据内容
	 */
	private DataSeries data = new DataSeries();
	
	public void initLayoutContent(int cols){
		this.layoutcontent = new ArrayList<List<LayoutData>>();
		/**
		 * 默认初始化的表格元素为 2行 3列
		 */
		if("table".equals(this.type)){
			layoutcontent.add(new ArrayList<LayoutData>()) ;
		}
		layoutcontent.add(new ArrayList<LayoutData>()) ;
		layoutcontent.add(new ArrayList<LayoutData>()) ;
		for(int rows=0 ; rows<layoutcontent.size() ; rows++){
			List<LayoutData> col = layoutcontent.get(rows) ;
			for(int i=0 ; i<cols ; i++){
				LayoutData layoutData = new LayoutData() ;
				if(rows == 0 && "table".equals(this.type)){
					layoutData.setHead(true) ;
				}
				col.add(layoutData) ;
			}
		}
	}
	
	public SearchResultTemplet getTemplet(){
		SearchResultTemplet templet = null ;
		if(this.templetid!=null){
			templet = (SearchResultTemplet) CacheHelper.getDistributedDictionaryCacheBean().getCacheObject(this.templetid, this.orgi) ;
		}
		return templet ;
	}
	
	private Map<String , Object> parameters = new HashMap<String, Object>();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTempletid() {
		return templetid;
	}
	public void setTempletid(String templetid) {
		this.templetid = templetid;
	}
	public Map<String, Object> getParameters() {
		return parameters;
	}
	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getPkgtype() {
		return pkgtype;
	}
	public void setPkgtype(String pkgtype) {
		this.pkgtype = pkgtype;
	}
	public boolean isPkglayout() {
		return pkglayout;
	}
	public void setPkglayout(boolean pkglayout) {
		this.pkglayout = pkglayout;
	}
	public DataSeries getData() {
		return data;
	}
	public void setData(DataSeries data) {
		this.data = data;
	}
	public ChartProperties getChart() {
		return chart;
	}
	public void setChart(ChartProperties chart) {
		this.chart = chart;
	}
	public List<List<LayoutData>> getLayoutcontent() {
		return layoutcontent;
	}
	public void setLayoutcontent(List<List<LayoutData>> layoutcontent) {
		this.layoutcontent = layoutcontent;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isDisplaytitle() {
		return displaytitle;
	}

	public void setDisplaytitle(boolean displaytitle) {
		this.displaytitle = displaytitle;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getWidthunit() {
		return widthunit;
	}

	public void setWidthunit(String widthunit) {
		this.widthunit = widthunit;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getHeightunit() {
		return heightunit;
	}

	public void setHeightunit(String heightunit) {
		this.heightunit = heightunit;
	}

	public Map<String, Object> getValues() {
		return values;
	}

	public void setValues(Map<String, Object> values) {
		this.values = values;
	}

	public String getHtmlcontent() {
		return htmlcontent;
	}

	public void setHtmlcontent(String htmlcontent) {
		this.htmlcontent = htmlcontent;
	}
}
