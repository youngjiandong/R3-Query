package com.rivues.module.datamodel.web.handler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.handler.Handler;
import com.rivues.module.platform.web.handler.RequestData;
import com.rivues.module.platform.web.handler.ResponseData;
import com.rivues.module.platform.web.interceptor.LogIntercreptorHandler;
import com.rivues.util.tools.RuntimeData;
  
@Controller  
@RequestMapping("/{orgi}/dm")  
public class DataModelController extends Handler{  
	private final Logger log = LoggerFactory.getLogger(LogIntercreptorHandler.class); 
    @RequestMapping(value="/index" , name="index" , type="dm",subtype="welcome")
    public ModelAndView index(HttpServletRequest request , @PathVariable String orgi,@Valid RuntimeData data) throws Exception{  
    	return new DatabaseController().index(request, orgi);
    }
} 