﻿// *************************************************
// Author	Bill
// Data		2013年12月6日
// *
(function($) {
	$.fn.plusSelect = function(options) {
		//定义要用的参数
		var opts = {
			opt_1 : '.selectauto',
			opt_2 : '#'+$(this).attr('id')+' a',
			opt_3 : '#'+$(this).attr('id')+' em',
			opt_4 : 'input[name='+$(this).attr('id')+']',
			onclick: function(){}

		};
		//合并参数供下面调用
		var opt = $.extend(opts, options);
		//fn内容开始

		this.each(function() {
			var _obj = $(this);
			_obj.click(function(e) {
				//console.log(_obj);
				e.stopPropagation();
				$(opt.opt_1).css({'marginLeft':'-9999px'});
				_obj.find(opt.opt_1).css({'marginLeft':'0'});
			})
		});

		$(document).bind("click", function() {
			$(opt.opt_1).css({'marginLeft':'-9999px'});
		});
		// console.log($(opt.opt_2));
		$(opt.opt_2).each(function(i){
			$(this).on('click', function(e){
				//console.log(e.target);
				e.stopPropagation();
				var thisHtml = $(this).html();
				$(opt.opt_3).html(thisHtml);
				$(opt.opt_1).css({'marginLeft':'-9999px'});
				$(this).addClass("cur").siblings().removeClass("cur");
				$(opt.opt_4).val($(this).attr('Svalue'));
				opt.onclick();
				
			})
		});

	}
})(jQuery);

$(function(){	
	
	$(".lx1").plusSelect({opt_2:'.lx1 li',opt_3:'.lx1 em',opt_4: "input[name=materia1]"});
	$(".lx2").plusSelect({opt_2:'.lx2 li',opt_3:'.lx2 em',opt_4: "input[name=materia2]"});
	$(".lx3").plusSelect({opt_2:'.lx3 li',opt_3:'.lx3 em',opt_4: "input[name=materia3]"});
	$(".lx4").plusSelect({opt_2:'.lx4 li',opt_3:'.lx4 em',opt_4: "input[name=materia4]"});
	
	$(".shtitle").each(function(i) {
		$('.shtitle').eq(i).css({"z-index":90-i});
	});
	
});